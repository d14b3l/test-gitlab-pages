from unittest import TestCase

import calc


class TestCalcModule(TestCase):
    def TestAddMethod(self):
        calculator = calc.Calc()
        self.assertEqual(calculator.add(1,4), 5)

    def TestGetName(self):
        calculator = calc.Calc()
        self.assertIsInstance(calculator.get_module_name(), str)