from setuptools import setup

setup(name='calc',
      version='0.1',
      description='simple module to play with coverage on gitlab pages',
      url='https://gitlab.com/d14b3l/test-gitlab-pages',
      author='d14b3l',
      author_email='a@q.put',
      license='??',
      packages=['calc'],
      zip_safe=False,
      test_suite='nose.collector',
      tests_require=['nose', 'rednose', 'nose-timer', 'coverage'])
